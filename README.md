# Chess Circuit

## Programa para evaluar el Circuito Provincial de Malaga 2023

Ante la complejidad de elaborar los resultados finales del Circuito Provincial de Malaga.
Debemos de recurrir a automatizar ciertas tareas.

Lo ideal, seria poner el fichero generado por Vega o Swiss Manager y que extrajera los datos e incorporarlos de forma automatica.
El inconveniente es que no hay unificacion en los patrones de elaboracion por parte arbitral. O haya muchas excepciones no contempladas.

Lo primero es crear un Login o Token para cada arbitro o responsable. (Log de accesos)
Un superadministrador o root.

Trabajaremos con una base de datos remota o en la Nube. Con ello facilitaremos el acceso a todos los arbitros y la informacion en tiempo real.
Por supuesto con permisos para cada arbitro y evento.


1. Torneo  
2. URL del Vega o fichero directo.
3. Si todo el mundo va a tener ID, trabajar directamente con ese ID, Posteriormente hacer conversiones con Id Fide o Feda,
4. Condicionales: Minimo de torneos, Federado.
5. Criterios de filtrados para los listados (querys).

Copias de seguridad.
Exportacion a otros formatos. csv, sql, pdf ...



