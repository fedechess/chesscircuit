-- MariaDB dump 10.19  Distrib 10.6.12-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: chesscircuit
-- ------------------------------------------------------
-- Server version	10.6.12-MariaDB-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `torneos`
--

DROP TABLE IF EXISTS `torneos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneos` (
  `id` int(11) NOT NULL,
  `torneo` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `municipio` varchar(100) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneos`
--

LOCK TABLES `torneos` WRITE;
/*!40000 ALTER TABLE `torneos` DISABLE KEYS */;
INSERT INTO `torneos` VALUES (1,'Alozaina','2023-05-14',NULL,'10:30:00','Alozaina','Pabellon Municipal'),(2,'Villanueva de Algaidas','2023-06-04',NULL,'10:00:00','Villanueva de Algaidas','Casa de la Cultura'),(3,'Almachar','2023-06-09',NULL,'17:00:00','Almachar','Parque Cultural María Zambrano'),(4,'Archidona','2023-06-24',NULL,'10:00:00','Archidona','Edificio \"El Silo\"'),(5,'Arriate','2023-06-25',NULL,'10:30:00','Arriate','Centro Socio Cultural \"La Pacheca\"'),(6,'Alora','2023-07-01',NULL,'10:00:00','Alora','CEIP Los Llanos'),(7,'Antequera','2023-07-08',NULL,'10:00:00','Antequera','Pabellón Cubierto Fernando Argüelles'),(8,'Monda','2023-07-15',NULL,'10:00:00','Monda','Biblioteca municipal'),(9,'Algarrobo ','2023-07-23',NULL,'10:00:00','Algarrobo ','Parque de la Escalerilla. Algarrobo'),(10,'Iznate ','2023-07-29',NULL,'10:00:00','Iznate ','CEIP Marqués de Iznate'),(11,'Teba ','2023-07-30',NULL,'10:00:00','Teba ','Pabellón Municipal de Teba'),(12,'Benamocarra','2023-08-05',NULL,'10:00:00','Benamocarra','Salón usos múltiples CEIP Eduardo Ocón Rivas'),(13,'Villanueva de la Concepción','2023-08-06',NULL,'10:00:00','Villanueva de la Concepción','Pabellón Municipal Isabel María Rodríguez Lozano \"ITA\"'),(14,'Coín ','2023-08-12',NULL,'10:00:00','Coín ','Centro Comercial La Trocha'),(15,'Casabermeja','2023-08-19',NULL,'10:00:00','Casabermeja','Plaza Fuente de Abajo'),(16,'Ojén','2023-08-26',NULL,'20:00:00','Ojén','Plaza Andalucía'),(17,'Rincón de la Victoria','2023-09-02',NULL,'10:00:00','Rincón de la Victoria','Plaza Al-Ándalus'),(18,'Mollina','2023-09-03',NULL,'10:00:00','Mollina','Salón de actos SCAA Virgen de la Oliva'),(19,'Nerja','2023-09-17',NULL,'16:00:00','Nerja','Sala Mercado / Pabellón Enrique López Cuenca'),(20,'Frigiliana','2023-09-23',NULL,'10:00:00','Frigiliana','Pabellón Villa de Frigiliana'),(21,'Marbella','2023-10-24',NULL,'10:00:00','Marbella','Carpa Municipal San Pedro de Alcántara'),(22,'Campillos','2023-10-01',NULL,'10:00:00','Campillos','Pabellón Antonio Rueda'),(23,'Cútar','2023-10-07',NULL,'10:00:00','Cútar','Antiguo cine'),(24,'Istán','2023-10-14',NULL,'10:00:00','Istán','Teatro Municipal'),(25,'Pizarra','2023-10-22',NULL,'10:00:00','Pizarra','Pabellón Municipal Dani Pacheco'),(26,'Mijas','2023-10-28',NULL,'17:00:00','Mijas','Cortijo Don Elías');
/*!40000 ALTER TABLE `torneos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-22 17:34:31
