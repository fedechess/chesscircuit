Diputación de Málaga | Servicio de Deporte | Sección Deporte Competición

C/ Pacífico, 54. 29004 (Málaga) | 952 069 177 | apaniagua@malaga.es | www.malaga.es/deportes

XXIX Circuito Provincial de Ajedrez

Diputación de Málaga 2023

Normativa
Como consecuencia del concierto en interés común para la promoción del deporte, entre la Diputación Provincial de Málaga y los Ayuntamientos de la provincia, se convoca el XXIX Circuito Provincial de Ajedrez Diputación de Málaga 2023, cuyos principales objetivos son dar satisfacción a la demanda existente y promocionar esta modalidad durante la época estival, mediante la organización de torneos en los diferentes municipios de Málaga.
1. Organización
La organización la llevará a cabo el Servicio de Deporte de la Diputación de Málaga, los Ayuntamientos organizadores de los diferentes torneos y la Federación Andaluza de Ajedrez (FADA), mediante la Delegación Malagueña, como entidad prestadora de servicios encomendados.
Aquellos municipios que no estén integrados en el Plan Provincial de Asistencia Económica y Cooperación 2023 y deseen adherirse al XXIX Circuito Provincial de Ajedrez de la Diputación de Málaga, podrán hacerlo abonando la cantidad correspondiente (750,00 €) a la Federación Andaluza de Ajedrez (en adelante FADA).

2. Competencias de las entidades implicadas

2.1 Diputación de Málaga

-Dirección, promoción y coordinación general.
-Elaboración de imagen y relaciones externas del circuito: material de cartelería, ruedas de prensa, convocatoria a los Ayuntamientos y contenidos de organización técnica.
-Material necesario para el desarrollo de los torneos.
-Difusión general de la actividad: página web y redes sociales.
-Gastos generales de organización a través del Plan Provincial de Asistencia y Cooperación 2023.

2.2 Ayuntamientos adheridos
-Coordinación local de la actividad.
-Promoción y difusión del torneo en se ámbito. En dicha difusión se hará explícita la referencia de su integración en el XXIX Circuito Provincial de Diputación de Málaga | Servicio de Deporte | Sección Deporte Competición C/ Pacífico, 54. 29004 (Málaga) | 952 069 177 | apaniagua@malaga.es | www.malaga.es/deportes Ajedrez, incorporando la imagen corporativa de la Diputación de Málaga y utilizando la cartelería facilitada el Servicio de Deporte.
-Elaboración de cartel del evento según modelo e indicaciones establecidas por la Diputación Provincial de Málaga.
-Envío de cartel e información sobre su torneo al menos 21 deas antes de la celebración al Servicio de Deporte y a la FADA, mediante documento facilitado por ésta, con los aspectos que queden abiertos en las bases y que sean necesarios concretar en función de las características propias de cada municipio.
-Trofeos y premios de su torneo.
-Adecuación y montaje del recinto de juego. Los Ayuntamientos procederán al montaje delimitando la zona de juego.
-Cobro de las inscripciones.
-Voluntarios para el desarrollo del torneo.
-Equipo informático de repuesto y/o apoyo al que aporta la FADA. 

2.3 Federación Andaluza de Ajedrez (FADA)
-Dirección técnica, elaboración del programa y montaje de los torneos, así como su desarrollo.
-Gestión de inscripciones y liquidación con el municipio organizador de la cuota de 2 euros por cada jugador no federado.
-Gestión y publicación de las clasificaciones de cada torneo y del ranking del Circuito.
-Elaboración de la plantilla de concreción de la normativa técnica del Circuito, para recabar toda la información necesaria para cada torneo.
-Plataforma web de competición.
-Arbitrajes. Un árbitro principal en cada torneo y árbitros auxiliares dependiendo de la participación.
-Equipo informático para su uso en cada torneo.
-Asesoramiento en la adecuación y montaje del recinto de juego, al objeto de garantizar el correcto desarrollo de cada torneo.
-Entrega de los premios generales, financiados por la Diputación de Málaga a través del Plan Provincial de Cooperación y Asistencia Municipal 2023.
-Remitir el informe técnico del desarrollo de los torneos al Servicio de Deportes, al término de cada uno de ellos, mediante el modelo de ficha habilitado para tal finalidad.
-Elaboración de una memoria – crónica de cada torneo, que incluya fotografías del evento y que recoja los resultados, que será puesta a disposición de los municipios que lo soliciten.
-Transporte y custodia del material de competición.
-Otras que se le encomienden.
3. Normativa técnica de competición y organización

3.1. Adhesión al circuito.
Todo club o entidad que desee que su torneo forme parte del Circuito Provincial de Ajedrez, deberá solicitarlo a la Diputación de Málaga a través de su Ayuntamiento dentro del Plan Provincial de Asistencia Económica y Cooperación. Aquellos municipios que no estén integrados dentro del Plan de Cooperación y Asistencia Municipal 2023 y deseen adherirse al XXIX Circuito Provincial de Ajedrez de la Diputación de Málaga, podrán hacerlo cumplimentando el formulario de adhesión y abonando la cantidad correspondiente a la Delegación Malagueña de Ajedrez.

3.2. Participantes:
Cualquier persona puede participar, sin condición de edad, sexo o nacionalidad, cumpliendo los requisitos de inscripción que en cada caso se establezca. El número de participantes estará condicionado por las características del espacio de juego, fijándose un máximo de 150 jugadores.

3.3. Categorías:
Se establecen las siguientes categorías:
Categoría Género Año nacimiento
Sub-8
Masculino
Femenino
2015 en adelante
Sub-10 2013 en adelante
Sub-12 2011 en adelante
Sub-14 2009 en adelante
Sub-16 2007 en adelante
Sub-18 2005 en adelante
Absoluto Anterior a 2005

3.4. Inscripciones:
Las inscripciones se canalizarán exclusivamente a través de las páginas web del servicio de Deporte de la Diputación de Málaga, https://www.malaga.es/deportes/ficha/cnl-558/ajedrez y la Delegación Malagueña de Ajedrez, https://ajedrezmalaga.org/ mediante un formulario habilitado para cada torneo.
El plazo de inscripción finalizará 24 horas antes del inicio de cada torneo o cuando se completen las plazas disponibles en cada caso. A partir de esta edición, no se admitirán inscripciones el mismo día del torneo.
El precio de inscripción será establecido por cada Ayuntamiento y abonado al responsable designado por éste, teniendo en cuenta que la Delegación Malagueña de Ajedrez percibirá 2 euros por jugador no federado, cantidad que será destinada al seguro deportivo y licencia de transeúnte.
El municipio organizador definirá si el pago previo es obligatorio o no, y en función de su decisión, indicará la forma de pago, sea in situ (metálico, TPV, etc.) o pago telemático previo (transferencia bancaria, pasarela de pago, etc.) o ambas opciones si así lo desea el organizador.
-Confirmación de inscripción:
No será preciso recibir confirmación de inscripción. Se considerará firme la inscripción del jugador que aparezca en el listado de inscritos, independientemente de que aparezca o no en el enlace de “info64”. Si se diera el caso de que un jugador inscrito en la web de la DMDA, no apareciera en “info64” el día del torneo, el equipo arbitral tendrá la obligación de incluirlo en el torneo.

3.5. Plataforma de competición (Lista de inscritos, publicación de horarios e información de interés.

La FADA, a través de la Delegación Malagueña de Ajedrez, habilitará una plataforma de competición, donde podrá consultarse toda la información relacionada con el circuito y con los torneos (lista de inscritos, horarios, clasificación, información turística del municipio, etc.)
Todas las personas que figuren como inscritos en la plataforma, estarán oficialmente inscritos en el torneo y tendrán asegurada su participación.
Los emparejamientos de cada torneo serán publicados en la web https://info64.org/ siguiendo el siguiente protocolo: el emparejamiento de la primera ronda de cada torneo se realizará 5-10 minutos antes de la hora de inicio,con los jugadores que se hayan inscrito en el plazo establecido y que no hayan cancelado de alguna forma su inscripción.

3.6. Ausencia de jugadores inscritos

Los jugadores inscritos a una prueba que por cualquier motivo no puedan asistir, tendrán la obligación de comunicarlo a la organización y justificarlo, de la forma siguiente:
3.6.1.- Comunicación de ausencias hasta 24 horas antes de la prueba.
En el formulario de inscripción al torneo habilitado en la página web de la Delegación Malagueña de Ajedrez deberá utilizar la opción de darse de baja (www.ajedrezmalaga.org).

3.6.2.- Comunicación de ausencias con menos de 24 horas.

Deberá dirigir un correo electrónico a ajedrezmalaga@gmail.com adjuntando en él, la documentación oportuna que justifique dicha ausencia. 
Las ausencias no comunicadas a la organización o no justificadas correctamente, tendrán una penalización automática para el jugador en cuestión, que conllevará la exclusión en los siguientes dos torneos. Si se diera el caso de 3 incomparecencias a lo largo del Circuito, el jugador en cuestión perdería el derecho a inscribirse en ningún torneo en lo que quede de Circuito.

3.7. Sistema de competición

Sistema suizo de 7 rondas y ritmo de juegos 8’+3” por jugada.

3.8. Arbitraje

Los torneos serán dirigidos por un árbitro principal y árbitros auxiliares cuando sean necesarios. Todos estarán titulados y pertenecerán a la Federación Andaluza de Ajedrez.

3.9. Sistema de puntuación y ranking.

En cada torneo se establecerá una clasificación individual por categoría.
Igualmente, se elaborará un ranking o clasificación general con la suma de los puntos obtenidos en los torneos en los que se participe. Dicha clasificación general se irá publicando periódicamente en las páginas web de la Delegación Malagueña de Ajedrez (www.ajedrezmalaga.org) y del Servicio de Deportes de la Diputación Provincial de Málaga.
Todos los torneos serán válidos para ELO FADA. El coste será asumido por la Delegación.

3.10. Premios del circuito

3.10.1. Premios por torneo.
Se fijan un mínimo de 12 trofeos por torneo: 2 por cada categoría sub (uno para cada género). En Total 12 trofeos “sub” y 6 trofeos absolutos (3 masculinos y 3 femeninos)
En caso de prever el organizador que algún trofeo pueda quedar vacante, podría contemplar en las bases la reasignación de los premios vacantes según el criterio que estime oportuno (según sea esta decisión deberá tenerlo en cuenta a
la hora de poner el nombre a los trofeos, colocando en la inscripción alguna denominación genérica).
A estos trofeos la organización podrá añadir otros premios y trofeos si lo desea (categoría local, premios en metálico, medallas, vales, tarjetas regalo, etc.)
En Cualquier caso, partiendo de estos mínimos establecidos, se especificará en la planilla de cada torneo, todos los premios que se entregarán. 

3.10.2. Premios generales del circuito (clasificación general)

La Diputación de Málaga, a través de la Delegación Malagueña de Ajedrez, establece para el presente año, un número de premios que dependerá del número de torneos celebrados.
Para poder acceder a los premios establecidos en este circuito se debe participar, al menos, en siete de los torneos convocados. La suma de los 7 mejores resultados nos dará la clasificación definitiva.
La clasificación general vendrá determinada por la suma de los puntos de los mejores 7 resultados y, en caso de empate, se tomarán los siguientes mejores resultados de los jugadores empatados, torneo a torneo hasta llegar al desempate.
Categoría Clasificación Premio

Absoluta
1º Masculino y femenino Trofeo + 200,00 € (*)
2º Masculino y femenino Trofeo + 140,00 € (*)
3º Masculino y femenino Trofeo + 110,00 € (*)

Sub-18
1º Masculino y femenino Trofeo +70,00 € (Cheque regalo**)
2º Masculino y femenino Trofeo
3º Masculino y femenino Trofeo

Sub-16
1º Masculino y femenino Trofeo + 50,00€ (Cheque regalo**)
2º Masculino y femenino Trofeo
3º Masculino y femenino Trofeo

Sub-14
1º Masculino y femenino Trofeo + 50,00€ (Cheque regalo**)
2º Masculino y femenino Trofeo
3º Masculino y femenino Trofeo

Sub-12
1º Masculino y femenino Trofeo + 30,00€ (Cheque regalo**)
2º Masculino y femenino Trofeo
3º Masculino y femenino Trofeo

Sub-10
1º Masculino y femenino Trofeo + 20,00€ (Cheque regalo**)
2º Masculino y femenino Trofeo
3º Masculino y femenino Trofeo

Sub-8 1º Masculino y femenino Trofeo + 20,00€ (Cheque regalo**)
2º Masculino y femenino Trofeo
3º Masculino y femenino Trofeo

Medalla conmemorativa a todos/as los que hayan participado en, al menos, en 7 de los torneos convocados.
Para poder acceder a los premios establecidos en este circuito se debe participar, al menos, en siete de los torneos convocados. La suma de los 7 mejores resultados nos dará la clasificación definitiva.
(*) En virtud de lo dispuesto en la Ley 35/2006. Art. 17; Ley 37/1992, Art. 4, 7 y 78. Al tratarse de una contraprestación económica en una actividad profesional, se aplicará el tipo de retención del 15% sobre los ingresos íntegros satisfechos.
(**) Ningún menor (inferior a sub-16) recibirá premios en metálico, sino el valor equivalente que correspondiera, en vales canjeables por material deportivo o similar, aunque dicho menor fuera acreedor al premio por su resultado en laclasificación general o absoluta.

4. Programa general
Los torneos tendrán una duración de medio día. Estableciéndose el siguiente calendario.

4.1. Calendario del circuito

FECHA HORA MUNICIPIO LUGAR
1 14/05/2023 10:30 Alozaina Pabellón municipal
2 04/06/2023 10:00 Villanueva de Algaidas Casa de la Cultura
3 09/06/2023 17:00 Almáchar Parque Cultural María Zambrano
4 24/06/2023 10:00 Archidona Edificio "El Silo"
5 25/06/2023 10:30 Arriate Centro Sociocultural "La Pacheca"
6 01/07/2023 10:00 Alora CEIP Los Llanos
7 08/07/2023 10:00 Antequera Pabellón cubierto Fernando Argüelles
8 15/07/2023 10:00 Monda Biblioteca municipal
9 23/07/2023 10:00 Algarrobo Parque de la Escalerilla. Algarrobo
10 29/07/2023 10:00 Iznate CEIP Marqués de Iznate
11 30/07/2023 10:00 Teba Pabellón municipal de Teba
12 05/08/2023 10:00 Benamocarra Salón usos múltiples CEIP Eduardo Ocón Rivas
13 06/08/2023 10:00 Villanueva de la Concepción Pabellón municipal Isabel María Rodríguez Lozano "ITA"
14 12/08/2023 10:00 Coín Centro comercial La Trocha
15 19/08/2023 10:00 Casabermeja Plaza Fuente de Abajo
16 26/08/2023 20:00 Ojén Plaza Andalucía
17 02/09/2023 10:00 Rincón de la Victoria Plaza Al-Ándalus
18 03/09/2023 10:00 Mollina Salón de actos SCAA Virgen de la Oliva
19 17/09/2023 16:00 Nerja Sala Mercado / Pabellón Enrique López Cuenca
20 23/09/2023 10:00 Frigiliana Pabellón Villa de Frigiliana
21 24/09/2023 10:00 Marbella Carpa municipal San Pedro de Alcántara
22 01/10/2023 10:00 Campillos Pabellón Antonio Rueda
23 07/10/2023 10:00 Cútar Antiguo cine
24 14/10/2023 10:00 Istán Teatro municipal
25 22/10/2023 10:00 Pizarra Pabellón municipal Dani Pacheco
26 28/10/2023 17:00 Mijas Cortijo Don Elías

5. CONTACTO

La Organización podrá publicar imágenes y demás datos de la actividad, con fines oportunos de publicidad, promoción, informes, etc. De acuerdo con lo previsto en la normativa vigente sobre Protección de datos de carácter personal, cualquier interesado puede ejercitar los derechos de acceso, rectificación, cancelación, y oposición, mediante un escrito ante el registro General de la Diputación de Málaga, o a través de la Oficina de Atención a la Ciudadanía, C/ Pacífico 54, 20004, Málaga. Málaga, a 10 de mayo de 2023

DIPUTACIÓN PROVINCIAL DE MÁLAGA

Servicio de Deporte
Alberto Paniagua
Teléfono: 952 069 177
E-mail: apaniagua@malaga.es

FEDERACIÓN ANDALUZA DE AJEDREZ
Delegación malagueña
Pablo Guirado
Teléfono: 687 842 314
E-mail: ajedrezmalaga@gmail.com
